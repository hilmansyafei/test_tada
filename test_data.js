'use strict';

//Question 1
/*Please repeat a name as many time as the characters in the name For example, Edi edi edi*/

const repeatName = (name) => {
	for (var i = 0, len = name.length; i < len; i++) {
		console.log(name);
	}
}
repeatName('Edi');

//Question 2
/*With input of months and year, make a function to count how many days in a month*/

const getDay = (month,year) => {
	let leapYear;
	let days;
	if (year % 4 == 0){
		if (year % 100 == 0){
			if (year % 400 == 0){
				leapYear = true
			}else{
				leapYear = false;
			}
		}else{
			leapYear = true
		}
	}else{
		leapYear = false
	}

	if (month == 2) {
		if(leapYear){
			days = 29;
		}else{
			days = 28;
		}
	}else{
		if (month % 2 == 0) {
			days = 30;
		}else{
			days = 31;
		}
	}

	return days;
}

console.log(getDay(2,2016));


//Question 3
/*3. Make a function to print 1 - 100, if the number is a multiplication of 3, it will print "max". if it is a multiplication of 5, it will print "good", it will print "maxgood" if it is a multiplication of 3 and 5*/

const loopHundred = (number) => {
	for (var i = 1; i <= 100 ; i++) {
		if (i % 3 == 0 && i % 5 == 0) {
			console.log('maxgood');
		}else if(i % 3 == 0){
			console.log('max');
		}else if(i % 5 == 0){
			console.log('good');
		}else{
			console.log(i);
		}
	}
}

loopHundred(100);

//Question 4
/* Write a function to determine a word is an anagram of the first word.
for example the function will take "secure" as first input and "rescue" as second input, and will print true as the result
e.g. isAnagram(“secure”, “rescue”) → true
e.g. isAnagram(“conifers”, “fir cones”) → true
e.g. isAnagram(“google”, “facebook”) → false*/

const isAnagram = (firstWord,secondWord) => {
	let check = 'true';
	firstWord = firstWord.replace(" ","");
	secondWord = secondWord.replace(" ","");
	if (firstWord.length != secondWord.length) {
		check = 'false';
	}else{
		for (var i = 0; i < secondWord.length; i++) {
			if(firstWord.indexOf(secondWord[i]) == -1){
				check =  'false';
			}else{
				let position = firstWord.indexOf(secondWord[i]);
				firstWord = firstWord.substring(0,position) + firstWord.substring(position+1, firstWord.length);	
			}
		}
	}
	return check;
}

console.log(isAnagram('conifers','fir cones'));














































